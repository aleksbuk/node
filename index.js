const express = require('express');
const app = express();
const PORT = 8080;
const path = require('path');
const fs = require('fs');
const morgan = require('morgan');

app.listen(PORT, () => {
    console.log(`${PORT} is working`)
})
app.use(express.json());
app.use((req, res, next) => {
    console.log(req.method);
    console.log(req.url);
    next();
})


app.post('/api/files', (req, res) => {
    try {
        const filename = req.body.filename;
        const content = req.body.content;
        let regEx = new RegExp(/^[A-z1-9.]+\.(txt|log|json|xml|js|yaml)$/);
        if (filename && content) {
            fs.writeFile(`.${req.url}/${filename}`, content, (err) => {
                if (err) {
                    res.status(400).json({
                        message: 'Please specify `filename` parameter',
                    });
                } else {
                    res.status(200).json({ message: `File created successfully` });
                }
            });
        } else {
            if (!content) {
                res.status(400).json({
                    message: 'Please specify `content` parameter',
                })}
            else if (!regEx.test(req.body.filename)) {
                res.status(400).json({
                    message: `Please specify 'content' parameter`
                })
            }
            else res.status(400).json({
                    message: 'Please specify `filename` && `content` parameters',
                });
        }
    } catch (e) {
        res.status(500).json({
            message: 'Server error',
        });
    }
});

app.get('/api/files', (req, res) => {
    let files = fs.readdirSync('/api/files/')
    {
        try {
            fs.stat('/api/files', function (err) {

                if (err) {
                    res.status(200).json({
                        message: 'Success',
                        files: files,
                    });
                } else if (err.code === 'ENOENT') {
                    res.status(400).json({
                        message: 'Client error',
                    });
                }
            });
        } catch (error) {
            res.status(500).json({
                message: 'Server error',
            });
        }
    }
})

app.get(`/api/files/:filename`, (req, res) => {
    const filename = path.basename(req.url);
    const stats = fs.statSync(`./api/files/${filename}`);
    let stat = `${stats.birthtime}`;
    let readFile = fs.readFileSync(`./api/files/${filename}`, "utf-8")
    const path = `./${filename}`;
    try {
        if (fs.existsSync(path)) {
            console.error('correct')
        }
    } catch (err) {
        console.log(err)
    }
    res.status(200).json(
        {
            "message": "Success",
            "filename": filename,
            "content": readFile,
            "extension": path.extname(req.url),
            "uploadedDate": stat,
        }
    )
})
